package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.ReturnsBBean;
import model.entity.ReturnsB;
import model.jpaejb.ReturnsBFacade;

@Named
@SessionScoped
public class ReturnsBController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	ReturnsBFacade returnsBFacade;
	
	@Inject
	ReturnsBBean returnsBBean;
	
	public List<ReturnsB> getAll() {
		return returnsBFacade.findAll();
	}
	
	public int count() {
		return returnsBFacade.count();
	}
	
	public String delete(ReturnsB returnsB) {
		returnsBFacade.remove(returnsB);
		
		return null;
	}
	
	public String add() {
		
		ReturnsB newRetrun = new ReturnsB();
		
		newRetrun.setBorrowing(returnsBBean.getBorrowing());
		newRetrun.setDateReturn(returnsBBean.getDateReturn());
		newRetrun.setFine(returnsBBean.getFine());
		newRetrun.setPerson(returnsBBean.getPerson());

		returnsBFacade.create(newRetrun);
		
		return "returnsB";
	}
	
	public void add(ReturnsB newReturn) {
		returnsBFacade.create(newReturn);
	}
	
	public String edit(ReturnsB choosenReturn) {

		returnsBBean.setId(choosenReturn.getId());
		returnsBBean.setBorrowing(choosenReturn.getBorrowing());
		returnsBBean.setDateReturn(choosenReturn.getDateReturn());
		returnsBBean.setFine(choosenReturn.getFine());
		returnsBBean.setPerson(choosenReturn.getPerson());
		
		return "returnsBUpdate";
	}
	
	public String save() {
		ReturnsB newReturn = new ReturnsB();
	
		newReturn.setId(returnsBBean.getId());
		newReturn.setBorrowing(returnsBBean.getBorrowing());
		newReturn.setDateReturn(returnsBBean.getDateReturn());
		newReturn.setFine(returnsBBean.getFine());
		newReturn.setPerson(returnsBBean.getPerson());
		
		returnsBFacade.edit(newReturn);
		
		return "returnsB";
	}
}
