package controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.entity.Borrowing;
import model.entity.Copy;
import model.entity.Title;

@Named
@SessionScoped
public class LibBorrowController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	BorrowingController borrowingController;
	
	public List<Borrowing> getNotReturnedBorrowings() {
		return borrowingController.getNotReturnedBorrowings();
	}
	
	public List<Copy> getAvaibleCopy(Title title) {

		List<Copy> copies = title.getCopies();
		List<Copy> avaibleCopies = new ArrayList<Copy>();
		List<Borrowing> borrows = getNotReturnedBorrowings();
		
		boolean avaible;
		for(Copy copy : copies) {
			avaible = true;
			for(Borrowing borrow : borrows) {
				if(borrow.getCopy().getId() == copy.getId() && borrow.getReturnsBs().isEmpty()) {
					avaible = false;
					break;
				}
			}
			
			if(avaible) {
				avaibleCopies.add(copy);
			}
		}
		
		return avaibleCopies;
	}
	

	public int howManyAvaibleCopies(Title title) {
		int avaibleCopies = getAvaibleCopy(title).size();
		int reservedCopies = title.getReservations().size();
		
		return avaibleCopies - reservedCopies;
	}
	
	public boolean isTitleAvaible(Title title) {
		return howManyAvaibleCopies(title) > 0 ? true : false;
	}
	
	public boolean canRemoveTitle(Title title) {
		int numOfCopies = title.getCopies().size();
		
		return numOfCopies > 0 ? false : true;
	}
}
