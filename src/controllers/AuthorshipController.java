package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.AuthorshipBean;
import model.entity.Authorship;
import model.jpaejb.AuthorshipFacade;

@Named
@SessionScoped
public class AuthorshipController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	AuthorshipFacade authorshipFacade;
	
	@Inject
	AuthorshipBean authorshipBean;
	
	public List<Authorship> getAll() {
		return authorshipFacade.findAll();
	}
	
	public int count() {
		return authorshipFacade.count();
	}
	
	public void delete(Authorship authorship) {
		authorshipFacade.remove(authorship);
	}
	
	public String add() {
		Authorship authorship = new Authorship();
	
		authorship.setAuthor(authorshipBean.getAuthor());
		authorship.setTitle(authorshipBean.getTitle());
		
		authorshipFacade.create(authorship);
		
		return "authorship";
	}
	
	public String edit(Authorship authorship) {
		
		authorshipBean.setId(authorship.getId());
		authorshipBean.setAuthor(authorship.getAuthor());
		authorshipBean.setTitle(authorship.getTitle());
		
		return "authorshipUpdate";
	}
	
	public String save() {
		Authorship authorship = new Authorship();
		
		authorship.setId(authorshipBean.getId());
		authorship.setAuthor(authorshipBean.getAuthor());
		authorship.setTitle(authorshipBean.getTitle());
		
		authorshipFacade.edit(authorship);
		
		return "authorship";
	}

}
