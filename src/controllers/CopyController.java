package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.CopyBean;
import model.entity.Borrowing;
import model.entity.Copy;
import model.jpaejb.CopyFacade;

@Named
@SessionScoped
public class CopyController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	CopyFacade copyFacade;
	
	@Inject
	CopyBean copyBean;
	
	@Inject
	TitleController titleController;
	
	public List<Copy> getAll() {
		return copyFacade.findAll();
	}
	
	public int count() {
		return copyFacade.count();
	}
	
	public void delete(Copy copy) {
		
		List<Borrowing> copyBorrowings = copy.getBorrowings();
		
		if(copyBorrowings.isEmpty()) {
			copyFacade.remove(copy);
			titleController.deleteTitleCopy(copy);
		}
	}
	
	public Copy add() {
		
		Copy copy = new Copy();
		
		copy.setNumCopy(copyBean.getNumCopy());
		copy.setTitle(copyBean.getTitle());
		copy.setNotes(copyBean.getNotes());
		
		copyFacade.create(copy);
		
		return copy;
	}
	
	public void edit(Copy copy) {

		copyBean.setId(copy.getId());
		copyBean.setNumCopy(copy.getNumCopy());
		copyBean.setTitle(copy.getTitle());
		copyBean.setBorrowings(copy.getBorrowings());
		copyBean.setNotes("");
	}
	
	public void save() {
		
		Copy copy = new Copy();
		
		copy.setId(copyBean.getId());
		copy.setNumCopy(copyBean.getNumCopy());
		copy.setTitle(copyBean.getTitle());
		copy.setNotes(copyBean.getNotes());
		copy.setBorrowings(copyBean.getBorrowings());
		
		copyFacade.edit(copy);
	}
}
