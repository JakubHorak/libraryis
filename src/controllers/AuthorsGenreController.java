package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.AuthorsGenreBean;
import model.entity.Authors_Genre;
import model.jpaejb.AuthorsGenreFacade;

@Named
@SessionScoped
public class AuthorsGenreController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	AuthorsGenreFacade authorsGenreFacade;
	
	@Inject
	AuthorsGenreBean authorsGenreBean;
	
	public List<Authors_Genre> getAll() {
		return authorsGenreFacade.findAll();
	}
	
	public int count() {
		return authorsGenreFacade.count();
	}
	
	public void delete(Authors_Genre authorGenre) {
		authorsGenreFacade.remove(authorGenre);
	}
	
	public String add() {
		Authors_Genre authorGenre = new Authors_Genre();
		
		authorGenre.setAuthor(authorsGenreBean.getAuthor());
		authorGenre.setGenre(authorsGenreBean.getGenre());

		authorsGenreFacade.create(authorGenre);
		
		return "authorsGenre";
	}
	
	public String edit(Authors_Genre authorGenre) {

		authorsGenreBean.setId(authorGenre.getId());
		authorsGenreBean.setAuthor(authorGenre.getAuthor());
		authorsGenreBean.setGenre(authorGenre.getGenre());
		
		return "authorsGenreUpdate";
	}
	
	public String save() {
		Authors_Genre authorGenre = new Authors_Genre();
	
		authorGenre.setId(authorsGenreBean.getId());
		authorGenre.setAuthor(authorsGenreBean.getAuthor());
		authorGenre.setGenre(authorsGenreBean.getGenre());
		
		authorsGenreFacade.edit(authorGenre);
		
		return "authorsGenre";
	}

}
