package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.TitlesGenreBean;
import model.entity.Titles_Genre;
import model.jpaejb.TitlesGenreFacade;

@Named
@SessionScoped
public class TitlesGenreController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	TitlesGenreFacade titlesGenreFacade;
	
	@Inject
	TitlesGenreBean titlesGenreBean;
	
	public List<Titles_Genre> getAll() {
		return titlesGenreFacade.findAll();
	}
	
	public int count() {
		return titlesGenreFacade.count();
	}
	
	public void delete(Titles_Genre titleGenre) {
		titlesGenreFacade.remove(titleGenre);
	}
	
	public String add() {
		Titles_Genre titleGenre = new Titles_Genre();
	
		titleGenre.setGenre(titlesGenreBean.getGenre());
		titleGenre.setTitle(titlesGenreBean.getTitle());
		
		titlesGenreFacade.create(titleGenre);
		
		return "titlesGenre";
	}
	
	public String edit(Titles_Genre titleGenre) {
		
		titlesGenreBean.setId(titleGenre.getId());
		titlesGenreBean.setGenre(titleGenre.getGenre());
		titlesGenreBean.setTitle(titleGenre.getTitle());
		
		return "titlesGenreUpdate";
	}
	
	public String save() {
		Titles_Genre titleGenre = new Titles_Genre();
		
		titleGenre.setId(titlesGenreBean.getId());
		titleGenre.setGenre(titlesGenreBean.getGenre());
		titleGenre.setTitle(titlesGenreBean.getTitle());
		
		titlesGenreFacade.edit(titleGenre);
		
		return "titlesGenre";
	}
}
