package controllers;

import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import controllers.users.PersonController;
import model.beans.BorrowingBean;
import model.entity.Borrowing;
import model.entity.Person;
import model.entity.ReturnsB;
import model.entity.Title;
import model.jpaejb.BorrowingFacade;
import model.jpaejb.PersonFacade;

@Named
@SessionScoped
public class BorrowingController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	BorrowingFacade borrowingFacade;
	
	@EJB
	PersonFacade personFacade;
	
	@Inject
	PersonController personController;
	
	@Inject
	ReturnsBController returnsBController;
	
	@Inject
	BorrowingBean borrowingBean;
	
	public List<Borrowing> getAll() {
		return borrowingFacade.findAll();
	}
	
	public int count() {
		return borrowingFacade.count();
	}
	
	public void delete(Borrowing borrowing) {
		List<ReturnsB> returns = borrowing.getReturnsBs();
		
		if(returns.isEmpty())
			borrowingFacade.remove(borrowing);
	}
	
	public String add() {
		Borrowing borrowing = new Borrowing();
		
		borrowing.setReader(borrowingBean.getReader());
		borrowing.setCopy(borrowingBean.getCopy());
		
		borrowing.setBorrowSince(new java.sql.Date(borrowingBean.getBorrowSince().getTime()));
		borrowing.setBorrowTO(new java.sql.Date(borrowingBean.getBorrowTO().getTime()));

		borrowing.setEmployee(personController.getLoggedPerson());
		borrowing.setReturnsBs(borrowing.getReturnsBs());
		borrowing.setIsReturn(false);
		
		borrowingFacade.create(borrowing);
		
		return "borrow";
	}
	
	public void prepareBorrow(Title title) {
		
		borrowingBean.setBorrowSince(getDate(0));
		borrowingBean.setBorrowTO(getDate(1));

		borrowingBean.setTitle(title);
		
		List<ReturnsB> emptyList = Collections.<ReturnsB>emptyList();
		borrowingBean.setReturnsBs(emptyList);		
	}
	
	public String edit(Borrowing borrowing) {

		borrowingBean.setId(borrowing.getId());
		borrowingBean.setBorrowSince(borrowing.getBorrowSince());
		borrowingBean.setBorrowTO(borrowing.getBorrowTO());
		borrowingBean.setCopy(borrowing.getCopy());
		borrowingBean.setEmployee(borrowing.getEmployee());
		borrowingBean.setIsReturn(borrowing.getIsReturn());
		borrowingBean.setReader(borrowing.getReader());
		borrowingBean.setReturnsBs(borrowing.getReturnsBs());
		
		borrowingFacade.edit(borrowing);
		
		return "borrowUpdate";
	}
	
	public void extendBorrowing(Borrowing borrowing) {

		// Add one month
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(borrowing.getBorrowTO());
		calendar.add(Calendar.MONTH, 1);
		borrowing.setBorrowTO(new Date(calendar.getTime().getTime()));
		
		// Change employee who extend borrowing
		borrowing.setEmployee(personController.getLoggedPerson());
		
		borrowingFacade.edit(borrowing);
	}
	
	public Date getDate(Calendar calendar) {
		return new Date(calendar.getTime().getTime());
	}
	
	public Date getDate(int addedMonths) {
		Calendar calendar = Calendar.getInstance();	
		calendar.add(Calendar.MONTH, addedMonths);
		
		return getDate(calendar);
	}
	
	public void save() {
		Borrowing borrow = new Borrowing();
	
		borrow.setId(borrowingBean.getId());
		borrow.setBorrowSince(new java.sql.Date(borrowingBean.getBorrowSince().getTime()));
		borrow.setBorrowTO(new java.sql.Date(borrowingBean.getBorrowTO().getTime()));
		borrow.setCopy(borrowingBean.getCopy());
		borrow.setEmployee(borrowingBean.getEmployee());
		borrow.setIsReturn(borrowingBean.getIsReturn());
		borrow.setReader(borrowingBean.getReader());
		borrow.setReturnsBs(borrowingBean.getReturnsBs());
		
		borrowingFacade.edit(borrow);
	}
	
	/**
	 * 
	 * @return Copy of borrowing.
	 */
	public Borrowing getCopyOfBorrow(Borrowing borrow) {
		Borrowing copyOfBorrowing = new Borrowing();
		
		copyOfBorrowing.setId(borrow.getId());
		copyOfBorrowing.setBorrowSince(borrow.getBorrowSince());
		copyOfBorrowing.setCopy(borrow.getCopy());
		copyOfBorrowing.setIsReturn(borrow.getIsReturn());
		copyOfBorrowing.setReader(borrow.getReader());
		copyOfBorrowing.setBorrowTO(borrow.getBorrowTO());
		copyOfBorrowing.setEmployee(borrow.getEmployee());
		
		return copyOfBorrowing;
	}
	
	/**
	 * 
	 * @param borrowing Borrowing which is returned.
	 */
	public void returnBorrowing(Borrowing borrowing) {
		ReturnsB newReturn = new ReturnsB();
		
		newReturn.setBorrowing(borrowing);
		
		Date today = getDate(0);
		newReturn.setDateReturn(today);
		
		Date borrowTo = borrowing.getBorrowTO();
		int fine = calculateFine(borrowTo, today);
		newReturn.setFine(fine);
		
		newReturn.setPerson(personController.getLoggedPerson());
		
		returnsBController.add(newReturn);
		
		changeBorrowingState(borrowing, newReturn);
	}
	
	private void changeBorrowingState(Borrowing borrowing, ReturnsB newReturn) {
		borrowing.addReturnsB(newReturn);
		borrowing.setIsReturn(true);
		
		borrowingFacade.edit(borrowing);
	}
	
	private int calculateFine(Date borrowTo, Date today) {
		int days = today.compareTo(borrowTo);
		
		return days * 10;
	}
	
	/**
	 * 
	 * @return Returns list of my borrowings.
	 */
	public List<Borrowing> getMyBorrowings() {
		Person person = personController.getLoggedPerson();
		
		return borrowingFacade.findPersonBorrowings(person);
	}
	
	public boolean giveReturned(Borrowing borrowing) {
		List<ReturnsB> returns = borrowing.getReturnsBs();
		
		return returns.isEmpty() ? false : true;
	}
	
	public List<Borrowing> getReturnedBorrowings() {
		return borrowingFacade.findReturnedBorrowings();
	}
	
	public List<Borrowing> getNotReturnedBorrowings() {
		return borrowingFacade.findNotReturnedBorrowings();
	}
	
}
