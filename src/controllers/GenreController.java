package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.GenreBean;
import model.entity.Genre;
import model.jpaejb.GenreFacade;

@Named
@SessionScoped
public class GenreController implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EJB
	GenreFacade genreFacade;
	
	@Inject
	GenreBean genreBean;
	
	public List<Genre> getAll() {
		return genreFacade.findAll();
	}
	
	public int count() {
		return genreFacade.count();
	}
	
	public void delete(Genre genre) {
		if(genre.getAuthorsGenres().isEmpty() && genre.getTitlesGenres().isEmpty())
			genreFacade.remove(genre);
	}
	
	public String add() {
		Genre genre = new Genre();
		
		genre.setName(genreBean.getName());

		genreFacade.create(genre);
		
		return "genres";
	}
	
	public String edit(Genre genre) {

		genreBean.setId(genre.getId());
		genreBean.setName(genre.getName());
		genreBean.setAuthorsGenres(genre.getAuthorsGenres());
		genreBean.setTitlesGenres(genre.getTitlesGenres());
		
		return "genresUpdate";
	}
	
	public String save() {
		Genre genre = new Genre();
	
		genre.setId(genreBean.getId());
		genre.setName(genreBean.getName());
		genre.setAuthorsGenres(genreBean.getAuthorsGenres());
		genre.setTitlesGenres(genreBean.getTitlesGenres());
		
		genreFacade.edit(genre);
		
		return "genres";
	}
}
