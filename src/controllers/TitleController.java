package controllers;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.CopyBean;
import model.beans.FindTitleBean;
import model.beans.TitleBean;
import model.entity.Copy;
import model.entity.Title;
import model.jpaejb.TitleFacade;

@Named
@SessionScoped
public class TitleController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	TitleFacade titleFacade;
	
	@Inject
	TitleBean titleBean;
	
	@Inject
	CopyController copyController;
	
	@Inject
	CopyBean copyBean;
	
	@Inject
	FindTitleBean findTitleBean;
	
	private List<Title> filteredTitles;
	
	public List<Title> getAll() {
		return titleFacade.findAll();	
	}
	
	public List<Title> getFilteredTitles() {
		return filteredTitles;	
	}
	
	public int count() {
		return titleFacade.count();
	}
	
	public String delete(Title title) {
		titleFacade.remove(title);

		return "librarianOverview";
	}
	
	public void deleteTitleCopy(Copy copy) {
		Title title = copy.getTitle();
		title.removeCopy(copy);
		
		edit(title);
		save();
	}
	
	public Title getOrCreate() {
		List<Title> titles = titleFacade.findTitle(
				titleBean.getIsbn());
		
		if(titles.isEmpty()) {
			add();
			titles = titleFacade.findTitle(
					titleBean.getIsbn());
		}
		
		return titles.get(0);
	}
	public String add() {
		
		Title title = new Title();
		
		title.setIsbn(titleBean.getIsbn());
		title.setEdition(titleBean.getEdition());
		title.setName(titleBean.getName());
		title.setLang(titleBean.getLang());
		title.setNumPages(titleBean.getNumPages());
		title.setAuthorships(null);
		title.setCopies(null);
		title.setReservations(null);
		title.setTitlesGenres(null);
		
		List <Title> seznam = titleFacade.findTitle(titleBean.getIsbn());
		
		if(seznam.isEmpty()) {
			titleFacade.create(title);
			return "librarianOverview";
		}
		
		warn();
		
		return "titleInsertForm";
	}
	
	/**
	 * @param title The title you want to create a copy.
	 * 
	 */
	public void addCopy(Title title) {
		
		copyBean.setBorrowings(null);
		copyBean.setTitle(title);
		copyBean.setNotes(null);
		
		int lastCopyNumber = makeLastCopyInt(title);
		copyBean.setNumCopy(lastCopyNumber);
		
		Copy newCopy = copyController.add();
		
		title.addCopy(newCopy);
		edit(title);
		save();
	}
	
	private int makeLastCopyInt(Title title) {
		List<Copy> copies = title.getCopies();
		int numOfCopies = copies.size() - 1;
		Copy lastCopy = copies.get(numOfCopies);
		int numOfLastCopy = lastCopy.getNumCopy();
		
		return  numOfLastCopy + 1;
	}
	
	public void edit(Title title) {

		titleBean.setIsbn(title.getIsbn());
		titleBean.setEdition(title.getEdition());
		titleBean.setName(title.getName());
		titleBean.setLang(title.getLang());
		titleBean.setNumPages(title.getNumPages());
		
		titleBean.setAuthorships(title.getAuthorships());
		titleBean.setCopies(title.getCopies());
		titleBean.setReservations(title.getReservations());
		titleBean.setTitlesGenres(title.getTitlesGenres());
	}

	public void save() {
		
		Title title = new Title();
		
		title.setIsbn(titleBean.getIsbn());
		title.setEdition(titleBean.getEdition());
		title.setName(titleBean.getName());
		title.setLang(titleBean.getLang());
		title.setNumPages(titleBean.getNumPages());
		
		title.setAuthorships(titleBean.getAuthorships());
		title.setCopies(titleBean.getCopies());
		title.setReservations(titleBean.getReservations());
		title.setTitlesGenres(titleBean.getTitlesGenres());
		
		titleFacade.edit(title);
	}

	public void warn() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Titul se zadanym ISBN jiz existuje!", ""));
    }

	public String findTitles(){
		
		filteredTitles = titleFacade.findTitlesForm(
				findTitleBean.getIsbn(),
				findTitleBean.getTitle(),
				findTitleBean.getLangs(),
				findTitleBean.getAuthors(), 
				findTitleBean.getGenres());
		
		return "index";
	}
}
