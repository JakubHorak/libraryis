package controllers;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.AuthorBean;
import model.entity.Author;
import model.jpaejb.AuthorFacade;

@Named
@SessionScoped
public class AuthorController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	AuthorFacade authorFacade;
	
	@Inject
	AuthorBean authorBean;
	
	public List<Author> getAll() {
		return authorFacade.findAll();
	}
	
	public int count() {
		return authorFacade.count();
	}
	
	public void delete(Author author) {
		if(author.getAuthorsGenres().isEmpty() && author.getAuthorships().isEmpty())
			authorFacade.remove(author);
	}
	
	public String add() {
		
		save();
		
		return back();
	}
	
	public String back() {
		
		authorBean.refresh();
		return "titles";
	}
	
	public String edit(Author author) {

		authorBean.setId(author.getId());
		authorBean.setFirstName(author.getFirstName());
		authorBean.setLastName(author.getLastName());
		authorBean.setBirthDate(author.getBirthDate());
		authorBean.setDateOfDeath(author.getDateOfDeath());
		authorBean.setInterest(author.getInterest());
		authorBean.setNationality(author.getNationality());
		authorBean.setAuthorsGenres(author.getAuthorsGenres());
		authorBean.setAuthorships(author.getAuthorships());
		
		return "authorInsertForm";
	}
	
	public void save() {
		
		Author author = new Author();
	
		author.setId(authorBean.getId());
		author.setFirstName(authorBean.getFirstName());
		author.setLastName(authorBean.getLastName());
		author.setBirthDate(authorBean.getBirthDate());
		author.setDateOfDeath(authorBean.getDateOfDeath());
		author.setInterest(authorBean.getInterest());
		author.setNationality(authorBean.getNationality());
		author.setAuthorsGenres(authorBean.getAuthorsGenres());
		author.setAuthorships(authorBean.getAuthorships());
		
		if(authorFacade.find(author.getId()) != null) {
			authorFacade.edit(author);
		}
		else {
			authorFacade.create(author);
		}
	}

}
