package controllers;

import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import controllers.users.PersonController;
import model.beans.ReservationBean;
import model.beans.login.Account;
import model.entity.Copy;
import model.entity.Person;
import model.entity.Reservation;
import model.entity.Title;
import model.jpaejb.BorrowingFacade;
import model.jpaejb.ReservationFacade;
import model.jpaejb.TitleFacade;

@Named
@SessionScoped
public class ReservationController implements Serializable {
	@EJB
	ReservationFacade reservationFacade;
	
	@EJB
	TitleFacade titleFacade;
	
	@EJB
	BorrowingFacade borrowingFacade;
	
	@Inject
	ReservationBean reservationBean;
	
	@Inject
	Account accountBean;
	
	@Inject
	PersonController personController;
	
	private static final long serialVersionUID = 1L;
	
	public List<Reservation> getAll() {
		return reservationFacade.findAll();
	}
	
	public List<Reservation> getMyReservations() {
		Person person = personController.getLoggedPerson();
		
		return getPersonReservations(person);
	}
	
	public int count() {
		return reservationFacade.count();
	}
	
	public void delete(Reservation reservation) {
		reservationFacade.remove(reservation);
	}
	
	public void deletem(Title title) {
		Person person = personController.getLoggedPerson();
		List<Reservation> userReservations = getPersonReservations(person, title);
		
		reservationFacade.remove(userReservations.get(0));
	}
	
	/**
	 * Add reservation for 3 days.
	 * 
	 * @param title Which title you want to reserve.
	 */
	public void add(Title title) {
		Reservation reservation = new Reservation();
		
		if(isTitleAvaible(title)) {
			// Get actual logged person (reader)
			reservation.setPerson(personController.getLoggedPerson());
			
			// Get actaul date a set it as reserve since
			Calendar calendar = Calendar.getInstance();
			reservation.setReserveSince(new Date(calendar.getTime().getTime()));
			
			// Set reservation for three days
			calendar.add(Calendar.DAY_OF_YEAR, 3);
			reservation.setReserveTo(new Date(calendar.getTime().getTime()));
			
			reservation.setTitle(title);
			
			reservationFacade.create(reservation);
			
			title.addReservation(reservation);
			
			titleFacade.edit(title);
		}
		
	}
	
	/**
	 * 
	 * @param title
	 * 
	 * @return true if title has reservations.
	 */
	public boolean hasReserve(Title title) {
		Person loggedUser = personController.getLoggedPerson();
		
		List<Reservation> userReservations = getPersonReservations(loggedUser, title);
		
		return userReservations.isEmpty() ? false : true;
	}
	
	public boolean hasNotReserved(Title title) {
		return hasReserve(title) ? false : true;
	}
	
	public String edit(Reservation reservation) {

		reservationBean.setId(reservation.getId());
		reservationBean.setPerson(reservation.getPerson());
		reservationBean.setReserveTo(reservation.getReserveTo());
		reservationBean.setTitle(reservation.getTitle());
		
		return "reservationUpdateForm";
	}

	public String save() {
		Reservation reservation = new Reservation();
		
		reservation.setId(reservationBean.getId());
		reservation.setPerson(reservationBean.getPerson());
		reservation.setReserveTo(reservationBean.getReserveTo());
		reservation.setTitle(reservationBean.getTitle());
		
		reservationFacade.edit(reservation);
		
		return "reservation";
	}
	
	public List<Reservation> getPersonReservations(Person person) {
		return reservationFacade.findMyReservations(person);
	}
	
	public List<Reservation> getPersonReservations(Person person, Title title) {
		return reservationFacade.findPersonReservations(person, title);
	}
	
	public boolean isTitleAvaible(Title title) {
		int allReservations = title.getReservations().size();
		
		List<Copy> copies= title.getCopies();
		int numOfCopies = copies.size();
		
		int avaibleCopies = numOfCopies - allReservations;
		
		boolean isBorrowed;
		for(Copy copy : copies) {
			isBorrowed = !borrowingFacade.findCopyBorrow(copy).isEmpty();
			if(isBorrowed) {
				avaibleCopies--;
			}
		}
		
		return avaibleCopies > 0 ? true : false;
	}
}
