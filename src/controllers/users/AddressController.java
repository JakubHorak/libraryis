package controllers.users;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.AddressBean;
import model.entity.Address;
import model.jpaejb.AddressFacade;

@Named
@SessionScoped
public class AddressController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	AddressFacade addressFacade;
	
	@Inject
	AddressBean addressBean;
	
	public Address getOrCreate() {
		
		Address address = addressFacade.findAddress(
				addressBean.getStreet(),
				addressBean.getPostCode(),
				addressBean.getCity());
		
		if(address == null) {
			address = add();
		}
		
		return address;
	}
	
	public String save() {
		
		Address address = new Address();
		
		address.setId(addressBean.getId());
		address.setStreet(addressBean.getStreet());
		address.setCity(addressBean.getCity());
		address.setPostCode(addressBean.getPostCode());
		address.setPersons(addressBean.getPersons());

		addressFacade.edit(address);
		
		return "address";
	}
	
	public Address add() {
		Address address = new Address();
		
		address.setStreet(addressBean.getStreet());
		address.setCity(addressBean.getCity());
		address.setPostCode(addressBean.getPostCode());
		address.setPersons(addressBean.getPersons());

		addressFacade.create(address);
		
		return address;
	}
	
	public String edit(Address address) {
		
		addressBean.setId(address.getId());
		addressBean.setStreet(address.getStreet());
		addressBean.setCity(address.getCity());
		addressBean.setPostCode(address.getPostCode());
		addressBean.setPersons(address.getPersons());
		
		return "addressUpdate";
	}
	
	public void delete(Address person) {
		if(person.getPersons().isEmpty())
			addressFacade.remove(person);
	}
	
	public List<Address> getAll() {
		return addressFacade.findAll();
	}
	
	public int count() {
		return addressFacade.count();
	}
}
