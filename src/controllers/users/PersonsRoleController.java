package controllers.users;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import model.beans.PersonBean;
import model.beans.PersonsRoleBean;
import model.beans.RoleBean;
import model.entity.Person;
import model.entity.Persons_Role;
import model.entity.Role;
import model.jpaejb.PersonsRoleFacade;
import model.jpaejb.RoleFacade;

@Named
@SessionScoped
public class PersonsRoleController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	PersonsRoleFacade personsRoleFacade;
	
	@EJB
	RoleFacade roleFacade;
	
	@Inject
	PersonsRoleBean personsRoleBean;
	
	@Inject
	PersonBean personBean;
	
	@Inject
	RoleBean roleBean;
	
	
	public List<Persons_Role> getPersonsRoles(Person person) {
		List<Persons_Role> allPersonRoles = getAll();
		List<Persons_Role> personRoles = new ArrayList<Persons_Role>();
		
		for(Persons_Role personRole : allPersonRoles) {
			if(personRole.getPerson().getLogin() == person.getLogin()) {
				personRoles.add(personRole);
			}
		}
		
		return personRoles;
	}
	
	public List<Persons_Role> getKnihovniky() {
		List<Persons_Role> allPersonRoles = getAll();
		List<Persons_Role> personRoles = new ArrayList<Persons_Role>();
		
		Role role;
		String description;
		for(Persons_Role personRole : allPersonRoles) {
			role = personRole.getRole();
			description = role.getDescription();
			
			if(description.equals("Knihovnik")) {
				personRoles.add(personRole);
			}
		}
		
		return personRoles;
	}
	
	
	
	public List<Persons_Role> getAll() {
		return personsRoleFacade.findAll();
	}
	
	public int count() {
		return personsRoleFacade.count();
	}
	
	public void delete(Persons_Role personRole) {
		
		Role role = personRole.getRole();
		Person person = personRole.getPerson();
		
		personsRoleFacade.remove(personRole);
		role.removePersonsRole(personRole);
		person.removePersonsRole(personRole);
	}
	
	public void add(Person person, Role role) {
		Persons_Role personRole = new Persons_Role();
		
		personRole.setPerson(person);
		personRole.setRole(role);
		
		personsRoleFacade.create(personRole);
	}
	
	public void nastavKnihovnika(Person person) throws IOException {
		
		List <Persons_Role> personRoles = personsRoleFacade.findAll();
		
		if(personRoles != null) {
			for(Persons_Role personRole : personRoles) {
				if(hasSameLogin(person, personRole) && hasLibrarianDesc(personRole)) {
					delete(personRole);
				}
					
			}
		}
		
		setPersonAsLibrarian(person);
	}
	
	private boolean hasSameLogin(Person person, Persons_Role personRole) {
		
		Person checkedPerson = personRole.getPerson();
		String checkedLogin = checkedPerson.getLogin();
		
		String personLogin = person.getLogin();
		
		return checkedLogin.equals(personLogin);
	}

	private boolean hasLibrarianDesc(Persons_Role personRole) {
		
		Role role = personRole.getRole();
		String description = role.getDescription();
		
		return description.equals("Knihovnik");
	}
	
	private void setPersonAsLibrarian(Person person) {
		
		List<Role> roles = roleFacade.findAll();
		String description;
		
		for(Role role : roles) {
			description = role.getDescription();
			if(description.equals("Knihovnik")) {
				add(person, role);
				break;
			}
		}
	}
	
	public String edit(Persons_Role personRole) {
		
		personsRoleBean.setId(personRole.getId());
		personsRoleBean.setPerson(personRole.getPerson());
		
		Role role = personRole.getRole();
		personsRoleBean.setRole(role);
		personsRoleBean.setRoleName(role.getCode());
		
		return "managmentUpdate";
	}
	
	public String save() {
		
		Persons_Role personRole = new Persons_Role();
		personRole.setId(personsRoleBean.getId());
		personRole.setPerson(personsRoleBean.getPerson());
		
		Role role = roleFacade.find(personsRoleBean.getRoleName());
		personRole.setRole(role);

		personsRoleFacade.edit(personRole);
		
		return "managment";
	}
	
}
