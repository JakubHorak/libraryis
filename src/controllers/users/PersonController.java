package controllers.users;

import java.io.Serializable;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.AddressBean;
import model.beans.PersonBean;
import model.entity.Person;
import model.entity.Persons_Role;
import model.entity.Role;
import model.jpaejb.AddressFacade;
import model.jpaejb.PersonFacade;
import model.jpaejb.RoleFacade;

@Named
@SessionScoped
public class PersonController implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EJB
	PersonFacade personFacade;
	
	@EJB
	AddressFacade addressFacade;
	
	@EJB
	RoleFacade roleFacade;
	
	@Inject
	PersonBean personBean;
	
	@Inject
	AddressBean addressBean;
	
	@Inject
	AddressController addressController;
	
	/**
	 * 
	 * @return Person Return logged person.
	 */
	public Person getLoggedPerson() {
		
		String loggedUsername = getLoggedUsername();
		
		Person person = personFacade.find(loggedUsername);
		
		return person;
	}
	
	private String getLoggedUsername() {

		String username = FacesContext.getCurrentInstance()
				.getExternalContext()
				.getUserPrincipal()
				.getName();
		
		return username;
	}
	
	public String add() {
		
		Person person = new Person();
		savePrivateData(person);
		
		// It's new person if date is null
		if(personBean.getDateRegistration() == null) {
			saveNewPersonData(person);
		}
		
		save(person);
		
		return back();
	}
	
	/**
	 * 
	 * @param person Person 
	 * 
	 * @return Person with updated data
	 */
	public Person savePrivateData(Person person) {
		
		// Personal info
		person.setLogin(personBean.getLogin());
		person.setFirstName(personBean.getFirstName());
		person.setLastName(personBean.getLastName());
		person.setPassword(personBean.getPassword());
		person.setBirthCode(personBean.getBirthCode());
		person.setEmail(personBean.getEmail());
		person.setPhone(personBean.getPhone());
		person.setAddress(addressController.getOrCreate());
		
		return person;
	}
	
	public Person saveNewPersonData(Person person) {
		
		person.setPassword(personBean.getLogin());
		person.setDateRegistration(new Date(Calendar.getInstance().getTime().getTime()));
		person.setPosition(null);
		person.setType("Ctenar");
		person.setEmplBorrows(null);
		person.setReadBorrows(null);
		person.setReservations(null);
		person.setReturnsBs(null);
		
		Persons_Role personRole = new Persons_Role();
		personRole.setPerson(person);
		
		Role role = roleFacade.find("reader");
		personRole.setRole(role);
		
		return person;
	}
	
	public void save(Person person) {
		String login = person.getLogin();
		Person foundedPerson = getPerson(login);
		
		if(foundedPerson != null) {
			personFacade.edit(person);
		}
		else {
			personFacade.create(person);
		}
	}
	
	public String back() {
		cleanBean();
		
		return "readers";
	}
	
	public void save() {
		
		Person person = new Person();
		
		// Personal info
		person.setLogin(personBean.getLogin());
		person.setFirstName(personBean.getFirstName());
		person.setLastName(personBean.getLastName());
		person.setPassword(personBean.getPassword());
		person.setBirthCode(personBean.getBirthCode());
		person.setEmail(personBean.getEmail());
		person.setPhone(personBean.getPhone());
		person.setAddress(personBean.getAddress());
		
		// Position in library
		person.setType(personBean.getType());
		person.setPosition(personBean.getPosition());
		person.setPersonsRoles(personBean.getPersonsRoles());
		person.setDateRegistration(personBean.getDateRegistration());
		
		// Person's obligation
		person.setEmplBorrows(personBean.getEmplBorrows());
		person.setReadBorrows(personBean.getReadBorrows());
		person.setReservations(personBean.getReservations());
		person.setReturnsBs(personBean.getReturnsBs());
		
		personFacade.edit(person);
	}
	
	public String edit(Person person) {
		
		// Personal info
		personBean.setLogin(person.getLogin());
		personBean.setFirstName(person.getFirstName());
		personBean.setLastName(person.getLastName());
		personBean.setPassword(person.getPassword());
		personBean.setBirthCode(person.getBirthCode());
		personBean.setEmail(person.getEmail());
		personBean.setPhone(person.getPhone());
		personBean.setAddress(person.getAddress());
		
		// Position in library
		personBean.setDateRegistration(person.getDateRegistration());
		personBean.setType(person.getType());
		personBean.setPosition(person.getPosition());
		personBean.setPersonsRoles(person.getPersonsRoles());
		
		// Person's obligation
		personBean.setEmplBorrows(person.getEmplBorrows());
		personBean.setReadBorrows(person.getReadBorrows());
		personBean.setReservations(person.getReservations());
		personBean.setReturnsBs(person.getReturnsBs());
		
		return "insertReaderForm";
	}
	
	public List<Person> getAll() {
		return personFacade.findAll();
	}
	
	public Person getPerson(String login) {
		return personFacade.find(login);
	}
	
	public List<Person> getAllReaders() {
		return personFacade.findAllReaders();
	}
	
	public int count() {
		return personFacade.count();
	}
	
	public void delete(Person person) {
		personFacade.remove(person);
	}
	
	public void cleanBean() {
		
		// Personal info
		personBean.setLogin(null);
		personBean.setFirstName(null);
		personBean.setLastName(null);
		personBean.setPassword(null);
		personBean.setBirthCode(0);
		personBean.setEmail(null);
		personBean.setPhone(null);
		personBean.setAddress(null);
		
		// Position in library
		personBean.setType(null);
		personBean.setPosition(null);
		personBean.setPersonsRoles(null);
		
		// Person's obligation
		personBean.setEmplBorrows(null);
		personBean.setReadBorrows(null);
		personBean.setReservations(null);
		personBean.setReturnsBs(null);
	}
}
