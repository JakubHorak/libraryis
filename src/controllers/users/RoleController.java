package controllers.users;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import model.beans.RoleBean;
import model.entity.Role;
import model.jpaejb.RoleFacade;

@Named
@SessionScoped
public class RoleController implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	RoleFacade roleFacade;
	
	@Inject
	RoleBean roleBean;
	
	public List<Role> getAll() {
		return roleFacade.findAll();
	}
	
	public int count() {
		return roleFacade.count();
	}
	
	public String delete(Role role) {
		roleFacade.remove(role);
		return null;
	}
	
	public String add() {
		
		Role role = new Role();
		
		role.setCode(roleBean.getCode());
		role.setDescription(roleBean.getDescription());
		
		roleFacade.create(role);
		
		return "role";
	}
	
	public String edit(Role role) {
		roleBean.setCode(role.getCode());
		roleBean.setDescription(role.getDescription());
		
		return "roleUpdate";
	}
	
	public String save() {
		Role role = new Role();
		role.setCode(roleBean.getCode());
		role.setDescription(roleBean.getDescription());

		roleFacade.edit(role);
		
		return "role";
	}
}
