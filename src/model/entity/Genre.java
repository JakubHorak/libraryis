package model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Genres")
@NamedQuery(name="Genre.findAll", query="SELECT g FROM Genre g")
public class Genre implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(nullable=false, length=20)
	private String name;

	//bi-directional many-to-one association to Authors_Genre
	@OneToMany(mappedBy="genre")
	private List<Authors_Genre> authorsGenres;

	//bi-directional many-to-one association to Titles_Genre
	@OneToMany(mappedBy="genre")
	private List<Titles_Genre> titlesGenres;

	public Genre() {
	}

	public Genre(int id_genre) {
		id = id_genre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Authors_Genre> getAuthorsGenres() {
		return authorsGenres;
	}

	public void setAuthorsGenres(List<Authors_Genre> authorsGenres) {
		this.authorsGenres = authorsGenres;
	}

	public Authors_Genre addAuthorsGenre(Authors_Genre authorsGenre) {
		getAuthorsGenres().add(authorsGenre);
		authorsGenre.setGenre(this);

		return authorsGenre;
	}

	public Authors_Genre removeAuthorsGenre(Authors_Genre authorsGenre) {
		getAuthorsGenres().remove(authorsGenre);
		authorsGenre.setGenre(null);

		return authorsGenre;
	}

	public List<Titles_Genre> getTitlesGenres() {
		return titlesGenres;
	}

	public void setTitlesGenres(List<Titles_Genre> titlesGenres) {
		this.titlesGenres = titlesGenres;
	}

	public Titles_Genre addTitlesGenre(Titles_Genre titlesGenre) {
		getTitlesGenres().add(titlesGenre);
		titlesGenre.setGenre(this);

		return titlesGenre;
	}

	public Titles_Genre removeTitlesGenre(Titles_Genre titlesGenre) {
		getTitlesGenres().remove(titlesGenre);
		titlesGenre.setGenre(null);

		return titlesGenre;
	}
}