package model.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Date;
import java.util.List;

@Entity
@Table(name="Borrowings")
@NamedQueries({
	@NamedQuery(name="Borrowing.findAll", query="SELECT b FROM Borrowing b"),
	@NamedQuery(name="Borrowing.findPersonBorrowings", query="SELECT b FROM Borrowing b WHERE b.reader = :reader AND b.isReturn = false"),
	@NamedQuery(name="Borrowing.findCopyBorrow", query="SELECT b FROM Borrowing b WHERE b.copy = :copy AND b.isReturn = false"),
	@NamedQuery(name="Borrowing.findNotReturnedBorrowings", query="SELECT b FROM Borrowing b WHERE b.returnsBs IS EMPTY"),
	@NamedQuery(name="Borrowing.findReturnedBorrowings", query="SELECT b FROM Borrowing b WHERE NOT b.returnsBs IS NOT EMPTY")
})
public class Borrowing implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(nullable=false)
	private Date borrowSince;

	@Column(nullable=false)
	private Date borrowTO;

	@Column(nullable=false)
	private boolean isReturn;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="employeeFk", nullable=false)
	private Person employee;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="readerFk", nullable=false)
	private Person reader;

	//bi-directional many-to-one association to Copy
	@ManyToOne
	@JoinColumn(name="copyFk", nullable=false)
	private Copy copy;

	//bi-directional many-to-one association to ReturnsB
	@OneToMany(mappedBy="borrowing")
	private List<ReturnsB> returnsBs;

	public Borrowing() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBorrowSince() {
		return borrowSince;
	}

	public void setBorrowSince(Date borrowSince) {
		this.borrowSince = borrowSince;
	}

	public void setBorrowSince(String borrowSince) {
		setBorrowSince(Date.valueOf(borrowSince));
	}
	
	public Date getBorrowTO() {
		return borrowTO;
	}

	public void setBorrowTO(Date borrowTO) {
		this.borrowTO = borrowTO;
	}
	
	public void setBorrowTO(String borrowTO) {
		setBorrowTO(Date.valueOf(borrowTO));
	}

	public boolean getIsReturn() {
		return isReturn;
	}

	public void setIsReturn(boolean isReturn) {
		this.isReturn = isReturn;
	}

	public Person getEmployee() {
		return employee;
	}

	public void setEmployee(Person employee) {
		this.employee = employee;
	}

	public Person getReader() {
		return reader;
	}

	public void setReader(Person reader) {
		this.reader = reader;
	}

	public Copy getCopy() {
		return copy;
	}

	public void setCopy(Copy copy) {
		this.copy = copy;
	}

	public List<ReturnsB> getReturnsBs() {
		return returnsBs;
	}

	public void setReturnsBs(List<ReturnsB> returnsBs) {
		this.returnsBs = returnsBs;
	}

	public ReturnsB addReturnsB(ReturnsB returnsB) {
		getReturnsBs().add(returnsB);
		returnsB.setBorrowing(this);

		return returnsB;
	}

	public ReturnsB removeReturnsB(ReturnsB returnsB) {
		getReturnsBs().remove(returnsB);
		returnsB.setBorrowing(null);

		return returnsB;
	}
}