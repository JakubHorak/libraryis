package model.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="Authors_Genres")
@NamedQuery(name="Authors_Genre.findAll", query="SELECT a FROM Authors_Genre a")
public class Authors_Genre implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	//bi-directional many-to-one association to Author
	@ManyToOne
	@JoinColumn(name="authorFk", nullable=false)
	private Author author;

	//bi-directional many-to-one association to Genre
	@ManyToOne
	@JoinColumn(name="genreFk", nullable=false)
	private Genre genre;

	public Authors_Genre() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

}