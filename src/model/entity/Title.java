package model.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;

@Entity
@Table(name="Titles")
@NamedQueries({
	@NamedQuery(name="Title.findAll", query="SELECT t FROM Title t"),
	@NamedQuery(name="Title.findTitle", query="SELECT a FROM Title a WHERE a.isbn = :isbn")
})	
public class Title implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(unique=true, nullable=false)
	private String isbn;

	@Column(nullable=false, length=10)
	private String edition;

	@Column(length=10)
	private String lang;

	@Column(nullable=false, length=100)
	private String name;

	@Column(nullable=false)
	private int numPages;

	//bi-directional many-to-one association to Authorship
	@OneToMany(mappedBy="title")
	private List<Authorship> authorships;

	//bi-directional many-to-one association to Copy
	@OneToMany(mappedBy="title")
	private List<Copy> copies;

	//bi-directional many-to-one association to Reservation
	@OneToMany(mappedBy="title")
	private List<Reservation> reservations;

	//bi-directional many-to-one association to Titles_Genre
	@OneToMany(mappedBy="title")
	private List<Titles_Genre> titlesGenres;

	public Title() {
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumPages() {
		return numPages;
	}

	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}	
	
	/* Pouze pro Formatovany vystup do tabulky */
	public String getAuthors() {
		String authors ="";
		boolean f = false;
		for(Authorship o: authorships) {
			if(f==false) {
				authors += o.getAuthor().getFirstName() + " " + o.getAuthor().getLastName();
				f=true;
			}
			else {
				authors+=",";
				authors+= o.getAuthor().getFirstName() + " " + o.getAuthor().getLastName();
			}
		}		
		return authors;
	}
	
	public List<Authorship> getAuthorships() {
		return authorships;
	}

	public void setAuthorships(List<Authorship> authorships) {
		this.authorships = authorships;
	}

	public Authorship addAuthorship(Authorship authorship) {
		getAuthorships().add(authorship);
		authorship.setTitle(this);

		return authorship;
	}

	public Authorship removeAuthorship(Authorship authorship) {
		getAuthorships().remove(authorship);
		authorship.setTitle(null);

		return authorship;
	}

	public List<Copy> getCopies() {
		return copies;
	}

	public void setCopies(List<Copy> copies) {
		this.copies = copies;
	}

	public Copy addCopy(Copy copy) {
		getCopies().add(copy);
		copy.setTitle(this);

		return copy;
	}

	public Copy removeCopy(Copy copy) {
		getCopies().remove(copy);
		copy.setTitle(null);

		return copy;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Reservation addReservation(Reservation reservation) {
		getReservations().add(reservation);
		reservation.setTitle(this);

		return reservation;
	}

	public Reservation removeReservation(Reservation reservation) {
		getReservations().remove(reservation);
		reservation.setTitle(null);

		return reservation;
	}

	public List<Titles_Genre> getTitlesGenres() {
		return titlesGenres;
	}
	
	public String getGenres() {
		String genres ="";
		boolean f = false;
		for(Titles_Genre o: titlesGenres){
			if(f==false) {
				genres += o.getGenre().getName();
				f=true;
			}
			else {
				genres+=",";
				genres+= o.getGenre().getName();
			}
		}
		
		return genres;
	}

	public void setTitlesGenres(List<Titles_Genre> titlesGenres) {
		this.titlesGenres = titlesGenres;
	}

	public Titles_Genre addTitlesGenre(Titles_Genre titlesGenre) {
		getTitlesGenres().add(titlesGenre);
		titlesGenre.setTitle(this);

		return titlesGenre;
	}

	public Titles_Genre removeTitlesGenre(Titles_Genre titlesGenre) {
		getTitlesGenres().remove(titlesGenre);
		titlesGenre.setTitle(null);

		return titlesGenre;
	}

}