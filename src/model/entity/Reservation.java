package model.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name="Reservations")
@NamedQueries({
	@NamedQuery (name="Reservation.findAll", query="SELECT r FROM Reservation r"),
	@NamedQuery(name="Reservation.findMyReservations", query="SELECT r FROM Reservation r WHERE r.person = :person"),
	@NamedQuery(name="Reservation.findPersonReservations", query="SELECT r FROM Reservation r WHERE r.person = :person AND r.title = :title")
})
public class Reservation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date reserveSince;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date reserveTo;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="loginFk", nullable=false)
	private Person person;

	//bi-directional many-to-one association to Title
	@ManyToOne
	@JoinColumn(name="isbnFk", nullable=false)
	private Title title;

	public Reservation() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getReserveSince() {
		return reserveSince;
	}

	public void setReserveSince(Date reserveSince) {
		this.reserveSince = reserveSince;
	}

	public Date getReserveTo() {
		return reserveTo;
	}

	public void setReserveTo(Date reserveTo) {
		this.reserveTo = reserveTo;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

}