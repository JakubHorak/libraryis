package model.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="Authorships")
@NamedQuery(name="Authorship.findAll", query="SELECT a FROM Authorship a")
public class Authorship implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	//bi-directional many-to-one association to Author
	@ManyToOne
	@JoinColumn(name="authorFk", nullable=false)
	private Author author;

	//bi-directional many-to-one association to Title
	@ManyToOne
	@JoinColumn(name="isbnFk", nullable=false)
	private Title title;

	public Authorship() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

}