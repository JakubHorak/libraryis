package model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Copies")
@NamedQueries({
	@NamedQuery(name="Copy.findAll", query="SELECT c FROM Copy c"),
	@NamedQuery(name="Copy.findAllTitleCopies", query="SELECT c FROM Copy c WHERE c.title = :isbnFk")
})
public class Copy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(length=100)
	private String notes;

	@Column(nullable=false)
	private int numCopy;

	//bi-directional many-to-one association to Borrowing
	@OneToMany(mappedBy="copy")
	private List<Borrowing> borrowings;

	//bi-directional many-to-one association to Title
	@ManyToOne
	@JoinColumn(name="isbnFk", nullable=false)
	private Title title;

	public Copy() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getNumCopy() {
		return numCopy;
	}

	public void setNumCopy(int numCopy) {
		this.numCopy = numCopy;
	}

	public List<Borrowing> getBorrowings() {
		return borrowings;
	}

	public void setBorrowings(List<Borrowing> borrowings) {
		this.borrowings = borrowings;
	}

	public Borrowing addBorrowing(Borrowing borrowing) {
		getBorrowings().add(borrowing);
		borrowing.setCopy(this);

		return borrowing;
	}

	public Borrowing removeBorrowing(Borrowing borrowing) {
		getBorrowings().remove(borrowing);
		borrowing.setCopy(null);

		return borrowing;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

}