package model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Authors")
@NamedQuery(name="Author.findAll", query="SELECT a FROM Author a")
public class Author implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Temporal(TemporalType.DATE)
	private Date birthDate;

	@Temporal(TemporalType.DATE)
	private Date dateOfDeath;

	@Column(nullable=false, length=20)
	private String firstName;

	@Column(length=50)
	private String interest;

	@Column(nullable=false, length=20)
	private String lastName;

	@Column(length=20)
	private String nationality;

	//bi-directional many-to-one association to Authors_Genre
	@OneToMany(mappedBy="author")
	private List<Authors_Genre> authorsGenres;

	//bi-directional many-to-one association to Authorship
	@OneToMany(mappedBy="author")
	private List<Authorship> authorships;

	public Author() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(Date dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}
	
	public String getFullName(){
		return firstName + " " + lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public List<Authors_Genre> getAuthorsGenres() {
		return authorsGenres;
	}

	public void setAuthorsGenres(List<Authors_Genre> authorsGenres) {
		this.authorsGenres = authorsGenres;
	}

	public Authors_Genre addAuthorsGenre(Authors_Genre authorsGenre) {
		getAuthorsGenres().add(authorsGenre);
		authorsGenre.setAuthor(this);

		return authorsGenre;
	}

	public Authors_Genre removeAuthorsGenre(Authors_Genre authorsGenre) {
		getAuthorsGenres().remove(authorsGenre);
		authorsGenre.setAuthor(null);

		return authorsGenre;
	}

	public List<Authorship> getAuthorships() {
		return this.authorships;
	}

	public void setAuthorships(List<Authorship> authorships) {
		this.authorships = authorships;
	}

	public Authorship addAuthorship(Authorship authorship) {
		getAuthorships().add(authorship);
		authorship.setAuthor(this);

		return authorship;
	}

	public Authorship removeAuthorship(Authorship authorship) {
		getAuthorships().remove(authorship);
		authorship.setAuthor(null);

		return authorship;
	}

}