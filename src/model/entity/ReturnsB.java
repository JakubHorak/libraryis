package model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="ReturnsB")
@NamedQuery(name="ReturnsB.findAll", query="SELECT r FROM ReturnsB r")
public class ReturnsB implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date dateReturn;

	private int fine;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="employeeFk", nullable=false)
	private Person person;

	//bi-directional many-to-one association to Borrowing
	@ManyToOne
	@JoinColumn(name="borrowingFk", nullable=false)
	private Borrowing borrowing;

	public ReturnsB() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateReturn() {
		return dateReturn;
	}

	public void setDateReturn(Date dateReturn) {
		this.dateReturn = dateReturn;
	}

	public int getFine() {
		return fine;
	}

	public void setFine(int fine) {
		this.fine = fine;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Borrowing getBorrowing() {
		return borrowing;
	}

	public void setBorrowing(Borrowing borrowing) {
		this.borrowing = borrowing;
	}

}