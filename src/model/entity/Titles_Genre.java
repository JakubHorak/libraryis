package model.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="Titles_Genres")
@NamedQuery(name="Titles_Genre.findAll", query="SELECT t FROM Titles_Genre t")
public class Titles_Genre implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	//bi-directional many-to-one association to Title
	@ManyToOne
	@JoinColumn(name="isbnFk", nullable=false)
	private Title title;

	//bi-directional many-to-one association to Genre
	@ManyToOne
	@JoinColumn(name="genreFk", nullable=false)
	private Genre genre;

	public Titles_Genre() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Title getTitle() {
		return title;
	}

	public void setTitle(Title title) {
		this.title = title;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

}