package model.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="Persons_Roles")
@NamedQueries({
	@NamedQuery(name="Persons_Role.findAll", query="SELECT p FROM Persons_Role p"),
	@NamedQuery(name="Persons_Role.findByPerson", query="SELECT p FROM Persons_Role p where p.person.login = :loginFk")
})
public class Persons_Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="roleFk", nullable=false)
	private Role role;

	//bi-directional many-to-one association to Person
	@ManyToOne
	@JoinColumn(name="loginFk", nullable=false)
	private Person person;

	public Persons_Role() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}