package model.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Date;
import java.util.List;


/**
 * The persistent class for the Persons database table.
 * 
 */
@Entity
@Table(name="Persons")
@NamedQueries({
	@NamedQuery(name="Person.findAll", query="SELECT p FROM Person p"),
	@NamedQuery(name="Person.findAllReaders", query="SELECT p FROM Person p WHERE p.type='Ctenar'")
})
public class Person implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(unique=true, nullable=false, length=12)
	private String login;

	@Column(nullable=false)
	private int birthCode;

	@Column
	private Date dateRegistration;

	@Column(nullable=false, length=20)
	private String email;

	@Column(nullable=false, length=20)
	private String firstName;

	@Column(nullable=false, length=20)
	private String lastName;

	@Column(nullable=false, length=512)
	private String password;

	@Column(nullable=false, length=9)
	private String phone;

	@Column(length=10)
	private String position;

	@Column(nullable=false, length=12)
	private String type;

	//bi-directional many-to-one association to Borrowing
	@OneToMany(mappedBy="employee")
	private List<Borrowing> emplBorrows;

	//bi-directional many-to-one association to Borrowing
	@OneToMany(mappedBy="reader")
	private List<Borrowing> readBorrows;

	//bi-directional many-to-one association to Address
	@ManyToOne
	@JoinColumn(name="addressFk", nullable=false)
	private Address address;

	//bi-directional many-to-one association to Persons_Role
	@OneToMany(mappedBy="person")
	private List<Persons_Role> personsRoles;

	//bi-directional many-to-one association to Reservation
	@OneToMany(mappedBy="person")
	private List<Reservation> reservations;

	//bi-directional many-to-one association to ReturnsB
	@OneToMany(mappedBy="person")
	private List<ReturnsB> returnsBs;

	public Person() {
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public int getBirthCode() {
		return birthCode;
	}

	public void setBirthCode(int birthCode) {
		this.birthCode = birthCode;
	}

	public Date getDateRegistration() {
		return dateRegistration;
	}

	public void setDateRegistration(Date dateRegistration) {
		this.dateRegistration =  dateRegistration;
;
	}
	
	public boolean getIsKnihovnik(){

		boolean flag=false;
		
		List <Persons_Role> j = getPersonsRoles();
		if(j!=null) {
			for(Persons_Role q: j) {
				if(q.getPerson().getLogin().equals(getLogin()) 
						&& q.getRole().getDescription().equals("Knihovnik")) {
					
					flag = true;
					break;
				}	
			}
		}
		
		return flag;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Borrowing> getEmplBorrows() {
		return emplBorrows;
	}

	public void setEmplBorrows(List<Borrowing> emplBorrows) {
		this.emplBorrows = emplBorrows;
	}

	public Borrowing addEmplBorrow(Borrowing emplBorrow) {
		getEmplBorrows().add(emplBorrow);
		emplBorrow.setEmployee(this);

		return emplBorrow;
	}

	public Borrowing removeEmplBorrow(Borrowing emplBorrow) {
		getEmplBorrows().remove(emplBorrow);
		emplBorrow.setEmployee(null);

		return emplBorrow;
	}

	public List<Borrowing> getReadBorrows() {
		return readBorrows;
	}

	public void setReadBorrows(List<Borrowing> readBorrows) {
		this.readBorrows = readBorrows;
	}

	public Borrowing addReadBorrow(Borrowing readBorrow) {
		getReadBorrows().add(readBorrow);
		readBorrow.setReader(this);

		return readBorrow;
	}

	public Borrowing removeReadBorrow(Borrowing readBorrow) {
		getReadBorrows().remove(readBorrow);
		readBorrow.setReader(null);

		return readBorrow;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Persons_Role> getPersonsRoles() {
		return personsRoles;
	}

	public void setPersonsRoles(List<Persons_Role> personsRoles) {
		this.personsRoles = personsRoles;
	}

	public Persons_Role addPersonsRole(Persons_Role personsRole) {
		getPersonsRoles().add(personsRole);
		personsRole.setPerson(this);

		return personsRole;
	}

	public Persons_Role removePersonsRole(Persons_Role personsRole) {
		getPersonsRoles().remove(personsRole);
		personsRole.setPerson(null);

		return personsRole;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Reservation addReservation(Reservation reservation) {
		getReservations().add(reservation);
		reservation.setPerson(this);

		return reservation;
	}

	public Reservation removeReservation(Reservation reservation) {
		getReservations().remove(reservation);
		reservation.setPerson(null);

		return reservation;
	}

	public List<ReturnsB> getReturnsBs() {
		return returnsBs;
	}

	public void setReturnsBs(List<ReturnsB> returnsBs) {
		this.returnsBs = returnsBs;
	}

	public ReturnsB addReturnsB(ReturnsB returnsB) {
		getReturnsBs().add(returnsB);
		returnsB.setPerson(this);

		return returnsB;
	}

	public ReturnsB removeReturnsB(ReturnsB returnsB) {
		getReturnsBs().remove(returnsB);
		returnsB.setPerson(null);

		return returnsB;
	}

}