package model.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Roles")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false, length=16)
	private String code;

	@Column(length=255)
	private String description;

	//bi-directional many-to-one association to Persons_Role
	@OneToMany(mappedBy="role")
	private List<Persons_Role> personsRoles;

	public Role() {
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Persons_Role> getPersonsRoles() {
		return personsRoles;
	}

	public void setPersonsRoles(List<Persons_Role> personsRoles) {
		this.personsRoles = personsRoles;
	}

	public Persons_Role addPersonsRole(Persons_Role personsRole) {
		getPersonsRoles().add(personsRole);
		personsRole.setRole(this);

		return personsRole;
	}

	public Persons_Role removePersonsRole(Persons_Role personsRole) {
		getPersonsRoles().remove(personsRole);
		personsRole.setRole(null);

		return personsRole;
	}

}