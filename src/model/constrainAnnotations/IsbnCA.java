package model.constrainAnnotations;
import static java.lang.annotation.ElementType.*;

import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import model.validator.*;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = IsbnValidator.class)
public @interface IsbnCA {
	
	String message() default "ISBN neni validni!";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
