package model.jpaejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Borrowing;
import model.entity.Copy;
import model.entity.Person;

@Stateless
public class BorrowingFacade extends AbstractFacade<Borrowing>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public BorrowingFacade() {
        super(Borrowing.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public List<Borrowing> findPersonBorrowings(Person p) {
		List<Borrowing> q = em.createNamedQuery("Borrowing.findPersonBorrowings", Borrowing.class)
				.setParameter("reader", p)
				.getResultList();
		return q;
	}
	
	public List<Borrowing> findCopyBorrow (Copy c) {
		List<Borrowing> q = em.createNamedQuery("Borrowing.findCopyBorrow", Borrowing.class)
				.setParameter("copy", c)
				.getResultList();
		return q;
	}
	
	public List<Borrowing> findNotReturnedBorrowings() {
		List<Borrowing> q = em.createNamedQuery("Borrowing.findNotReturnedBorrowings", Borrowing.class)
				.getResultList();
		return q;
	}
	
	public List<Borrowing> findReturnedBorrowings() {
		List<Borrowing> q = em.createNamedQuery("Borrowing.findReturnedBorrowings", Borrowing.class)
				.getResultList();
		return q;
	}
}
