package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

import model.entity.Address;

@Stateless
public class AddressFacade extends AbstractFacade<Address>{
	
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
	
    public AddressFacade() {
        super(Address.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public Address findAddress(String street, int postCode, String city) {
		
		try {
			Address address = em.createNamedQuery("Address.findAddress", Address.class)
					.setParameter("street", street)
					.setParameter("city", city)
					.setParameter("postCode", postCode)
					.getSingleResult();
			
			return address;
		}
		catch (NoResultException | NonUniqueResultException | IllegalStateException e) {
			return null;
		}
	}

}