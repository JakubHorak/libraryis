package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Authors_Genre;

@Stateless
public class AuthorsGenreFacade extends AbstractFacade<Authors_Genre>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public AuthorsGenreFacade() {
        super(Authors_Genre.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
