package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Genre;

/**
 * Session Bean implementation class GenreFacade
 */
@Stateless
public class GenreFacade extends AbstractFacade<Genre>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public GenreFacade() {
        super(Genre.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}
