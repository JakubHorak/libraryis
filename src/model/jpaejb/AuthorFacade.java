package model.jpaejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Author;

@Stateless
public class AuthorFacade extends AbstractFacade<Author> {
	
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    
    public AuthorFacade() {
        super(Author.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public List<Author> findAuthor(String first, String last) {
		List<Author> q = em.createQuery("SELECT a FROM Author a WHERE a.firstName = :firstName AND a.lastName = :lastName")
				.setParameter("firstName", first)
				.setParameter("lastName", last)
				.getResultList();
		return q;
	}
}
