package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Titles_Genre;

@Stateless
public class TitlesGenreFacade extends AbstractFacade<Titles_Genre>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public TitlesGenreFacade() {
        super(Titles_Genre.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
