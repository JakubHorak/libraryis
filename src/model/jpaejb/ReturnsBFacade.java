package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.ReturnsB;

@Stateless
public class ReturnsBFacade extends AbstractFacade<ReturnsB>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ReturnsBFacade() {
        super(ReturnsB.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
