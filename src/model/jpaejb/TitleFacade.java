package model.jpaejb;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Authorship;
import model.entity.Title;
import model.entity.Titles_Genre;

@Stateless
public class TitleFacade extends AbstractFacade<Title>{
	
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
	
    public TitleFacade() {
        super(Title.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public List<Title> findTitle(String isbn) {
		List<Title> foundedTitles = em.createNamedQuery("Title.findTitle", Title.class)
				.setParameter("isbn", isbn)
				.getResultList();
		
		return foundedTitles;
	}
	
	public List<Title> findTitlesForm(
			String isbn, 
			String name,
			List<String> langs, 
			List<String> authors, 
			List<String> genres) {
				
		String titleSQL = "SELECT a FROM Title a";
		
        
		if(!isbn.isEmpty() || !name.isEmpty() || !langs.isEmpty()) {
			titleSQL += addExtension(isbn, name, langs);
		}
	
		@SuppressWarnings("unchecked")
		List<Title> resultQuery = em.createQuery(titleSQL).getResultList();

		if(!genres.isEmpty() || !authors.isEmpty())
			return filterTitles(resultQuery, authors, genres);
		
		return resultQuery;
	}
	
	private String addExtension(String isbn, String name, List<String> langs) {
		String titleSQL = " WHERE ";
		boolean isAnd = false;
		
    	if(!langs.isEmpty()) {
    		titleSQL += addLangExtension(langs);
    		isAnd = true;
    	}
    	
    	if(!name.isEmpty()){
    		titleSQL += getFlag(isAnd);
    		titleSQL += "a.name='" + name + "'";
    		
    		isAnd = true;
    	}
    	
    	if(!isbn.isEmpty()){
    		titleSQL += getFlag(isAnd);
    		titleSQL += "a.isbn='" + isbn + "'";
    		
    		isAnd = true;
    	}
    	
    	return titleSQL;
	}
	
	private String addLangExtension(List<String> langs) {
		String titleSQL = "( a.lang='" + langs.get(0) + "'";
		
		if(langs.size() > 1) {
			for(String lang : langs) {
				titleSQL += " OR a.lang='" + lang + "'";
			}
		}
		
		titleSQL += ")";
		
		return titleSQL;
	}	
	
	private List<Title> filterTitles(List<Title> titles, List<String> authors, List<String> genres) {
		List<Title> filteredTitles = new ArrayList<Title>();
		
		if(!genres.isEmpty() && !authors.isEmpty()) {				
			for (Title title : titles) {
				if(checkGenre(genres, title) && checkAuthorship(authors, title)) {
						filteredTitles.add(title);
				}
			}
		}
		else if(!authors.isEmpty()) {				
			for (Title title: titles) {
				if(checkAuthorship(authors, title))
					filteredTitles.add(title);
			}
		}
		else if(!genres.isEmpty()) {
			for (Title title: titles){
				if(checkGenre(genres, title))
					filteredTitles.add(title);
			}
		}
		
		return filteredTitles;
	}
	
	private boolean checkGenre(List<String> genres, Title title) {
		
		List<Titles_Genre> titleGenres = title.getTitlesGenres();
		String genreName;
		
		for(Titles_Genre titleGenre : titleGenres) {
			genreName = titleGenre.getGenre().getName();
			
			if(containSample(genreName, genres))
					return true;
		}
			return false;
	}
	
	private boolean checkAuthorship(List<String> authors, Title title) {
		
		List<Authorship> authorships = title.getAuthorships();
		String authorFullName;
		
		for(Authorship authorship : authorships) {
			authorFullName = authorship.getAuthor().getFullName();
			
			if(containSample(authorFullName, authors))
				return true;
		}
		
		return false;
	}
	
	private boolean containSample(String sample, List<String> targets) {
		for(String target : targets) {
			if(target.equals(sample)) {
				return true;
			}
		}
		return false;
	}
		
	/*
	 * Function add AND to sql if needed
	 * 
	 * @param flag Is flag needed
	 * 
	 * @return String " AND " if is needed, otherwise empty string
	 */
	private String getFlag(boolean flag){
		return flag ? " AND " : "";    			
	}
}
