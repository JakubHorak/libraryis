package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Persons_Role;

@Stateless
public class PersonsRoleFacade extends AbstractFacade<Persons_Role>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public PersonsRoleFacade (){
        super(Persons_Role.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}
