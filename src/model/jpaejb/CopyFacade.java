package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Copy;


@Stateless
public class CopyFacade extends AbstractFacade<Copy>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public CopyFacade() {
        super(Copy.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
