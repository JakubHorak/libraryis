package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Authorship;

@Stateless
public class AuthorshipFacade extends AbstractFacade<Authorship>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public AuthorshipFacade() {
        super(Authorship.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
