package model.jpaejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Person;
import model.entity.Reservation;
import model.entity.Title;

@Stateless
public class ReservationFacade extends AbstractFacade<Reservation>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public ReservationFacade() {
        super(Reservation.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public List<Reservation> findMyReservations(Person user) {
		List<Reservation> q = em.createNamedQuery("Reservation.findMyReservations", Reservation.class)
				.setParameter("person", user)
				.getResultList();
		return q;
	}
	
	public List<Reservation> findPersonReservations(Person user, Title t) {
		List<Reservation> q = em.createNamedQuery("Reservation.findPersonReservations", Reservation.class)
				.setParameter("person", user)
				.setParameter("title", t)
				.getResultList();
		return q;
	}
}
