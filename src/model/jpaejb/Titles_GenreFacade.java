package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Titles_Genre;

/**
 * Session Bean implementation class Titles_GenreFacade
 */
@Stateless
public class Titles_GenreFacade extends AbstractFacade<Titles_Genre>{


	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public Titles_GenreFacade() {
        super(Titles_Genre.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}
