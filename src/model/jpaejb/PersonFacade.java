package model.jpaejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Person;

/**
 * Session Bean implementation class GenreFacade
 */
@Stateless
public class PersonFacade extends AbstractFacade<Person>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public PersonFacade() {
        super(Person.class);
    }

    public List<Person> findAllReaders() {
    	List<Person> q = em.createNamedQuery("Person.findAllReaders", Person.class)
				.getResultList();
		return q;
    }
    
    public List<Person> findAllEmployees() {
    	List<Person> q = em.createNamedQuery("Person.findAllEmployees", Person.class)
				.getResultList();
		return q;
    }
    
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}