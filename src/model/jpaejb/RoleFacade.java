package model.jpaejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.entity.Role;

@Stateless
public class RoleFacade extends AbstractFacade<Role>{
	@PersistenceContext(unitName = "blackbooks")
	private EntityManager em;
    /**
     * Default constructor. 
     */
    public RoleFacade() {
        super(Role.class);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
}
