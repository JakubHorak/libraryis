package model.validator;

import model.constrainAnnotations.*;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsbnValidator implements ConstraintValidator<IsbnCA, String> {

   
    public void initialize(IsbnCA constraintAnnotation) {
      
    }

    public boolean isValid(String isbn, ConstraintValidatorContext constraintContext) {
 
        if (isbn == null || isbn.length() < 10 || isbn.length() > 13) {
            return false;
        }

        return true;
    }
}