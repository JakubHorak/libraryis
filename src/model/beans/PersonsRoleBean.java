package model.beans;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Person;
import model.entity.Role;

@Named
@Stateless
public class PersonsRoleBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private Role role;
	private Person person;
	private String roleName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
