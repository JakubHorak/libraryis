package model.beans;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Persons_Role;
import model.entity.Role;
import model.jpaejb.RoleFacade;


@Named
@Stateless
public class RoleBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	RoleFacade roleFacade;
	
	private String code;
	private String description;
	private List<Persons_Role> personsRoles;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Persons_Role> getPersonsRoles() {
		return personsRoles;
	}
	public void setPersonsRoles(List<Persons_Role> personsRoles) {
		this.personsRoles = personsRoles;
	}
	public Persons_Role addPersonsRole(Persons_Role personsRole) {
		getPersonsRoles().add(personsRole);
		Role r = roleFacade.find(code);
		personsRole.setRole(r);

		return personsRole;
	}

	public Persons_Role removePersonsRole(Persons_Role personsRole) {
		getPersonsRoles().remove(personsRole);
		personsRole.setRole(null);

		return personsRole;
	}

}
