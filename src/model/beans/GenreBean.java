package model.beans;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Authors_Genre;
import model.entity.Titles_Genre;

@Named
@Stateless
public class GenreBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private List<Authors_Genre> authorsGenres;
	private List<Titles_Genre> titlesGenres;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Authors_Genre> getAuthorsGenres() {
		return authorsGenres;
	}

	public void setAuthorsGenres(List<Authors_Genre> authorsGenres) {
		this.authorsGenres = authorsGenres;
	}

	public List<Titles_Genre> getTitlesGenres() {
		return titlesGenres;
	}

	public void setTitlesGenres(List<Titles_Genre> titlesGenres) {
		this.titlesGenres = titlesGenres;
	}
}
