package model.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Copy;
import model.entity.Person;
import model.entity.ReturnsB;
import model.entity.Title;

@Named
@Stateless
public class BorrowingBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private Date borrowSince;
	private Date borrowTO;
	private boolean isReturn;
	private Person employee;
	private Person reader;
	private Copy copy;
	private Title title;

	private List<ReturnsB> returnsBs;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getBorrowSince() {
		return borrowSince;
	}
	
	public void setBorrowSince(Date borrowSince) {
		this.borrowSince = borrowSince;
	}

	public Date getBorrowTO() {
		return borrowTO;
	}
	
	public void setBorrowTO(Date borrowTO) {
		this.borrowTO = borrowTO;
	}

	public boolean getIsReturn() {
		return isReturn;
	}

	public void setIsReturn(boolean isReturn) {
		this.isReturn = isReturn;
	}

	public Person getEmployee() {
		return employee;
	}

	public void setEmployee(Person employee) {
		this.employee = employee;
	}

	public Person getReader() {
		return reader;
	}

	public void setReader(Person reader) {
		this.reader = reader;
	}

	public Copy getCopy() {
		return copy;
	}

	public void setCopy(Copy copy) {
		this.copy = copy;
	}

	public List<ReturnsB> getReturnsBs() {
		return returnsBs;
	}

	public void setReturnsBs(List<ReturnsB> returnsBs) {
		this.returnsBs = returnsBs;
	}

	public void setTitle(Title title) {
		this.title = title;
	}
	
	public Title getTitle() {
		return title;
	}
}
