package model.beans;


import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Genre;
import model.entity.Title;

@Named
@Stateless
public class TitlesGenreBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private Title title;
	private Genre genre;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Title getTitle() {
		return title;
	}
	public void setTitle(Title title) {
		this.title = title;
	}
	public Genre getGenre() {
		return genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	
}
