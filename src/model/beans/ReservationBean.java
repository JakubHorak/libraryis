package model.beans;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Person;
import model.entity.Title;

@Named
@Stateless
public class ReservationBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private Date reserveSince;
	private Date reserveTo;
	private Person person;
	private Title title;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getReserveSince() {
		return reserveSince;
	}
	public void setReserveSince(Date reserveSince) {
		this.reserveSince = reserveSince;
	}
	public Date getReserveTo() {
		return reserveTo;
	}
	public void setReserveTo(Date reserveTo) {
		this.reserveTo = reserveTo;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Title getTitle() {
		return title;
	}
	public void setTitle(Title title) {
		this.title = title;
	}
}
