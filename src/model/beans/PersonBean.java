package model.beans;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Address;
import model.entity.Borrowing;
import model.entity.Persons_Role;
import model.entity.Reservation;
import model.entity.ReturnsB;

@Named
@Stateless
public class PersonBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String login;
	private int birthCode;
	private Date dateRegistration;
	private String email;
	private String firstName;
	private String lastName;
	private String password;
	private String phone;
	private String position;
	private String type;
	private Address address;
	
	private List<Borrowing> emplBorrows;
	private List<Borrowing> readBorrows;
	
	private List<Persons_Role> personsRoles;
	private List<Reservation> reservations;
	private List<ReturnsB> returnsBs;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public int getBirthCode() {
		return birthCode;
	}
	public void setBirthCode(int birthCode) {
		this.birthCode = birthCode;
	}
	public Date getDateRegistration() {
		return dateRegistration;
	}
	public void setDateRegistration(Date dateRegistration) {
		this.dateRegistration = dateRegistration;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<Borrowing> getEmplBorrows() {
		return emplBorrows;
	}
	public void setEmplBorrows(List<Borrowing> emplBorrows) {
		this.emplBorrows = emplBorrows;
	}
	public List<Borrowing> getReadBorrows() {
		return readBorrows;
	}
	public void setReadBorrows(List<Borrowing> readBorrows) {
		this.readBorrows = readBorrows;
	}
	public List<Persons_Role> getPersonsRoles() {
		return personsRoles;
	}
	public void setPersonsRoles(List<Persons_Role> personsRoles) {
		this.personsRoles = personsRoles;
	}
	public List<Reservation> getReservations() {
		return reservations;
	}
	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	public List<ReturnsB> getReturnsBs() {
		return returnsBs;
	}
	public void setReturnsBs(List<ReturnsB> returnsBs) {
		this.returnsBs = returnsBs;
	}
	
}
