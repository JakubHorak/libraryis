package model.beans;

import java.io.Serializable;
import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Borrowing;
import model.entity.Person;

@Named
@Stateless
public class ReturnsBBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private Date dateReturn;
	private int fine;
	private Person person;
	private Borrowing borrowing;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDateReturn() {
		return dateReturn;
	}
	public void setDateReturn(Date dateReturn) {
		this.dateReturn = dateReturn;
	}
	public int getFine() {
		return fine;
	}
	public void setFine(int fine) {
		this.fine = fine;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Borrowing getBorrowing() {
		return borrowing;
	}
	public void setBorrowing(Borrowing borrowing) {
		this.borrowing = borrowing;
	}
}
