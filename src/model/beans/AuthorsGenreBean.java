package model.beans;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Author;
import model.entity.Genre;

@Named
@Stateless
public class AuthorsGenreBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private Author author;
	private Genre genre;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	public Genre getGenre() {
		return genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	
}
