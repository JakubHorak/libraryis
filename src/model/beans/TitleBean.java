package model.beans;

import java.io.Serializable;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.constrainAnnotations.IsbnCA;

import model.entity.Authorship;
import model.entity.Copy;
import model.entity.Reservation;
import model.entity.Titles_Genre;


@Named
@Stateless
public class TitleBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@IsbnCA // ISBN VALIDATOR
	private String isbn;
	
	private String edition;
	private String lang;
	private String name;
	private int numPages;

	private List<Authorship> authorships;
	private List<Copy> copies;
	private List<Reservation> reservations;
	private List<Titles_Genre> titlesGenres;
	
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumPages() {
		return numPages;
	}
	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}
	

	public List<Authorship> getAuthorships() {
		return authorships;
	}
	public void setAuthorships(List<Authorship> authorships) {
		this.authorships = authorships;
	}
	public List<Copy> getCopies() {
		return copies;
	}
	public void setCopies(List<Copy> copies) {
		this.copies = copies;
	}
	public List<Reservation> getReservations() {
		return reservations;
	}
	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	public List<Titles_Genre> getTitlesGenres() {
		return titlesGenres;
	}
	public void setTitlesGenres(List<Titles_Genre> titlesGenres) {
		this.titlesGenres = titlesGenres;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
