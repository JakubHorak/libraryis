package model.beans;

import javax.ejb.Stateless;
import javax.inject.Named;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Named
@Stateless
public class FindTitleBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String isbn;
	private String title;

	private String genre;
	private String author;
	private String lang;
	
	private List<String> genres = new ArrayList<String>();
	private List<String> authors = new ArrayList<String>();
	private List<String> langs = new ArrayList<String>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name != null ? name : "";
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn != null ? isbn : "";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title != null ? title : "";
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		if(genre != null)
			this.genres = Arrays.asList(genre.split("\\s*,\\s*"));
		else
			genres = Collections.emptyList();
	}

	public void setAuthor(String author) {
		if(author != null)
			authors = Arrays.asList(author.split("\\s*,\\s*"));
		else
			authors = Collections.emptyList();
	}
	
	public String getAuthor() {
		return author;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		if(lang != null)
			langs = Arrays.asList(lang.split("\\s*,\\s*"));
		else
			langs = Collections.emptyList();
	}

	public List<String> getGenres() {
		return genres;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public List<String> getLangs() {
		return langs;
	}
}
