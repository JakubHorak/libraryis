package model.beans.login;


import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Named
@SessionScoped
public class AccountDAO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Account account = new Account();
	private String errMsg = "";
	private boolean isLogged= false;
	
	public String getErrMsg() {
		return errMsg;
	}
	
	public Account getAccount() {
		return account;
	}
	
	private void getUserRole(HttpServletRequest request) {
		// Zjistim jakou ma uzivatel prirazenou roli v JAAC
		boolean isReader = request.isUserInRole(Roles.READER.toString());
		boolean isBoss = request.isUserInRole(Roles.BOSS.toString());
		boolean isLibrarian = request.isUserInRole(Roles.LIBRARIAN.toString());
		
		System.out.println(isReader);
		System.out.println(isBoss);
		System.out.println(isLibrarian);
		// Prenesu JAAC role uzivatele do objektu
		account.setReader(isReader);
		account.setBoss(isBoss);
		account.setLibrarian(isLibrarian);
	}
	
	public void login() {
		FacesContext context = FacesContext.getCurrentInstance();
	    HttpServletRequest request = (HttpServletRequest) 
	        context.getExternalContext().getRequest();
	    context.addMessage(null, new FacesMessage("Prihl " + account.getUsername()));
	    if(!isLogged) {
	    	context.addMessage(null, new FacesMessage("Zkousim se prihlasit " + account.getUsername()));
		    try {
		    	System.out.println("Prihlasuji se jako" + account.getUsername() + "s heslem " + account.getPassword());
		    	request.login(account.getUsername(), account.getPassword());
		    	isLogged = true;
		    	context.addMessage(null, new FacesMessage("Vitej " + account.getUsername()));
		    	getUserRole(request);
		    	System.out.println("Podarilo se");
		    } 
		    catch (ServletException e) {
		    	errMsg = "Login failed.";
		    	System.out.println("Nepodarilo se");
		    }
	    }
	}
	  
	public void logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) 
				context.getExternalContext().getRequest();
		
	    try {
	    	errMsg = "";
	    	isLogged = false;
	    	account = null;
	    	request.logout();
	    	FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
		    FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	    }
	    catch (ServletException e) {
	    	context.addMessage(null, new FacesMessage("Logout failed."));
	    }
	}
}
