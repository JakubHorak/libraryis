package model.beans.login;

public enum Roles {
	READER {
		@Override
		public String toString() {
			return "reader";
		}
	},
	BOSS {
		@Override
		public String toString() {
			return "boss";
		}
	},
	LIBRARIAN {
		@Override
		public String toString() {
			return "librarian";
		}
	},
	NONE {
		@Override
		public String toString() {
			return "none";
		}
	}

}
