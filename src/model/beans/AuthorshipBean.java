package model.beans;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Author;
import model.entity.Title;

@Named
@Stateless
public class AuthorshipBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;
	private Author author;
	private Title title;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Author getAuthor() {
		return author;
	}
	public void setAuthor(Author author) {
		this.author = author;
	}
	public Title getTitle() {
		return title;
	}
	public void setTitle(Title title) {
		this.title = title;
	}
	
	
}
