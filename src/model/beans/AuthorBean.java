package model.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.validation.constraints.Past;

import model.entity.Authors_Genre;
import model.entity.Authorship;

@Named
@Stateless
public class AuthorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	
	@Past
	private Date birthDate;
	
	@Past
	private Date dateOfDeath;
	private String firstName;
	private String interest;
	private String lastName;
	private String nationality;
	private List<Authors_Genre> authorsGenres = new ArrayList<Authors_Genre>();
	private List<Authorship> authorships = new ArrayList<Authorship>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Date getDateOfDeath() {
		return dateOfDeath;
	}
	public void setDateOfDeath(Date dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getInterest() {
		return interest;
	}
	public void setInterest(String interest) {
		this.interest = interest;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public List<Authors_Genre> getAuthorsGenres() {
		return authorsGenres;
	}
	public void setAuthorsGenres(List<Authors_Genre> authorsGenres) {
		this.authorsGenres = authorsGenres;
	}
	public List<Authorship> getAuthorships() {
		return authorships;
	}
	public void setAuthorships(List<Authorship> authorships) {
		this.authorships = authorships;
	}
	
	public void refresh() {
		id = -1;
		birthDate = null;
		dateOfDeath = null;
		firstName = null;
		interest = null;
		lastName = null;
		nationality = null;
		authorsGenres.clear();
		authorships.clear();
	}
}
