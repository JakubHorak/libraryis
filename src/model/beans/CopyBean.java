package model.beans;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;

import model.entity.Borrowing;
import model.entity.Title;

@Named
@Stateless
public class CopyBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String notes;
	private int numCopy;
	private Title title;

	private List<Borrowing> borrowings;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getNumCopy() {
		return numCopy;
	}
	public void setNumCopy(int numCopy) {
		this.numCopy = numCopy;
	}
	public List<Borrowing> getBorrowings() {
		return borrowings;
	}
	public void setBorrowings(List<Borrowing> borrowings) {
		this.borrowings = borrowings;
	}
	public Title getTitle() {
		return title;
	}
	public void setTitle(Title title) {
		this.title = title;
	}
}
