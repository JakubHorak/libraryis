/******************************************************************************
 * Project: Information system - Library
 * Package: info.db
 * File: DbConnection.java
 * Date:  21.11.2016
 * Last change: 22.11.2016
 * Author:  Jakub Horak xhorak61
 *
 * Desc: 	Class for creating and maintain database connection using JDBC
 * 			resources.
 *
 ******************************************************************************/

package info.db;

import java.sql.Connection;
import java.sql.SQLException;


import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @file DbConnection.java
 * 
 * @brieg Class for creating and maintain database connection.
 */
public class DbConnection {

	private InitialContext context;
    private Connection connection;
    
    /**
     * Constructor
     * 
     * @param context InitialContext, where to find JDBC names.
     * @param connection Database connection.
     */
    private DbConnection(InitialContext context, Connection connection) {
    	this.context = context;
    	this.connection = connection;
    }
    
	/**
	 * Create database connection.
     * 
     * @param JdbcResourcesName Name of JDBC resources.
     * 
     * @return If database connection cannot be created return null otherwise return database connection.
     */
    public static DbConnection getDbConnection(String JdbcResourcesName) {
		try {
			InitialContext context = new InitialContext();
			DataSource dataSource = (DataSource) context.lookup(JdbcResourcesName);
			Connection connection = dataSource.getConnection();
			
			return new DbConnection(context, connection);
			
		} catch (NamingException | SQLException e) {
			e.printStackTrace();
			
			return null;
			
		}
    }
    
    /**
     * 
     * @return Connection Database connection.
     */
    public Connection getConnection() {
    	return connection;
    }
    
    /**
     * Close context and database connection.
     * 
     */
    public void close() {
    	try {
    		context.close();
			connection.close();
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
		}
    }
}
